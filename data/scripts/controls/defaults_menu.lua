return {

  keyboard = {
    confirm = "return",
    confirm_2 = "space",
    cancel = "escape",
    cancel_2 = "backspace",

    up = "up",
    down = "down",
    left = "left",
    right = "right",
  },


  joypad = {
    confirm = "a",
    cancel = "b",
    right = "dpad_right",
    up = "dpad_up",
    left = "dpad_left",
    down = "dpad_down",
    --movement_stick = "left",
    submenu_left = "left_shoulder",
    submenu_right = "right_shoulder",
  },
}

