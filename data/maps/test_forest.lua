local map = ...
local game = map:get_game()


map:register_event("on_started", function()

end)


function bridge_a_switch:on_activated()
  for ent in map:get_entities("bridge_a") do
    ent:set_enabled(true)
  end
end

function switchblock_switch_a:on_activated()
  sol.audio.play_sound"door_open"
  switchblock_a:set_enabled(false)
end

function switchblock_switch_b:on_activated()
  sol.audio.play_sound"door_open"
  switchblock_b:set_enabled(false)
end

function end_sensor:on_activated()
  game:start_dialog("the_end", function()
    sol.main.reset()
  end)
end
