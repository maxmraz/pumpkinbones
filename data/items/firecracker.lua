local item = ...
local game = item:get_game()
local fuse_duration = 600
local initial_speed = 300
local speed_loss = 40

function item:on_started()
  item:set_savegame_variable("possession_firecracker")
  item:set_amount_savegame_variable("amount_firecracker")
  item:set_max_amount(25)
  item:set_amount(25)
  item:set_assignable(true)
end


function item:on_obtaining()
  item:add_amount(item:get_max_amount())
  game:set_item_assigned(1, item)
end
 

function item:on_using()
  local hero = game:get_hero()
  if item:has_amount(1) then
    item:remove_amount(1)
    local map = game:get_map()
    hero:freeze()
    sol.audio.play_sound("fuse_short")
    hero:set_animation("throwing", function()
      hero:set_animation"stopped"
      hero:unfreeze()
      item:set_finished()
    end)
    sol.timer.start(map, 60, function()
      sol.audio.play_sound"throw"
      local x, y, z = hero:get_position()
      local direction = hero:get_direction()
      local grenade = map:create_custom_entity{
        x=x, y=y, layer=z,
        width=16, height=16, direction = 0,
        sprite = "entities/firecracker",
      }
      local sprite = grenade:get_sprite()
      sprite:set_animation("falling")
      grenade:set_can_traverse("hero", true)
      local m = sol.movement.create"straight"
      m:set_speed(initial_speed)
      m:set_angle(direction * math.pi / 2)
      m:start(grenade)
      local elapsed_time = 0
      local step = 100
      sol.timer.start(grenade, step, function()
        elapsed_time = elapsed_time + step
        if elapsed_time < fuse_duration then
          m:set_speed(m:get_speed() - speed_loss)
          return true
        else
          x, y, z = grenade:get_position()
          grenade:remove()
          local explosion = map:create_explosion{
            x=x, y=y, layer=z,
          }
          sol.audio.play_sound"explosion"
        end
      end)
    end)
  else
    sol.audio.play_sound"wrong"
    hero:unfreeze()
    item:set_finished()
  end
end
